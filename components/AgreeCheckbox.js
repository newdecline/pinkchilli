import React from 'react';

export const AgreeCheckbox = ({
    field: {name, value, onChange},
    form: {errors},
    id,
    ...props
    }) => {
    return (
        <label
            htmlFor={id}
            className={errors[name]
                ? 'label-checkbox label-checkbox_error'
                : 'label-checkbox'}>
            <input
                id={id}
                name={name}
                value={value}
                checked={value}
                type="checkbox"
                onChange={onChange}
                className="input"/>
            <span className="square"> </span>
                Согласие с обработкой персональных данных
        </label>
    );
};