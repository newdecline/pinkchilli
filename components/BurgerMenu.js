import React from 'react';
import { useSelector, useDispatch  } from 'react-redux';
import {toggleMenu} from '../redux/actions/actionCreators';

export const BurgerMenu = () => {
    const isOpenMenu = useSelector(state => state.menu.isOpenMenu);
    const dispatch = useDispatch();

    return (

        <div
            className={isOpenMenu ? 'burger burger_active' : 'burger'}
            onClick={() =>  dispatch(toggleMenu())} >
            <span></span>
            <span></span>
            <span></span>
        </div>
    );
};