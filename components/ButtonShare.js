import React, {Component} from 'react';
import ShareIcon from "../svg/share-icon.svg";
import {
    FacebookShareButton,
    VKShareButton,
} from 'react-share';

export default class ButtonShare extends Component {
    state = {
        isOpenToolTip: false
    };
    onToggleOpenToolTip = () => {
        this.setState(state => ({isOpenToolTip: !state.isOpenToolTip}))
    };
    render() {
        const classes = {
            iconsShare: ['icons-share']
        };
        this.state.isOpenToolTip ? classes.iconsShare.push('icons-share_show') : null;
        return (
            <div className="wrapper-share">
                <ShareIcon
                    className="share-btn"
                    onClick={() => this.onToggleOpenToolTip()}
                    />
                <div className={classes.iconsShare.join(' ')}>
                    <FacebookShareButton url={`http://agency.pinkchilli.tech/statya${this.props.id}`} className="icon-share">
                        FACEBOOK
                    </FacebookShareButton>
                    <VKShareButton url={`http://agency.pinkchilli.tech/statya${this.props.id}`} className="icon-share">
                        VK
                    </VKShareButton>
                </div>
            </div>
        );
    }
};