import React from 'react';
import Link from 'next/link';
import Img from 'react-image';
import Preloader from "./Preloader";

const CartProject = props => {
    const {index, viewCart, project: {slug, previewSrc, title, description, alt}} = props;
    let linkStyle;
    let textStyle;

    viewCart === 'tile' ? linkStyle = {backgroundImage: `url(${previewSrc})`} : null;
    viewCart === 'tile' ? textStyle = {color: `${props.project.textColor}`} : null;

    return (
        <Link
          key={slug}
          href={{
            pathname: '/project',
            query: {
              slug
            }
          }}
          as={`/proekty/${slug}`}
        >
            <a
                data-aos={viewCart === 'list' ? 'fade-up' : null}
                data-aos-delay={`${(index + 1) * 50}`}
                data-aos-offset="50"
                className="wrap-cart"
                style={linkStyle}
            >
            <div className="wrapper-img">
                <Img
                    src={previewSrc}
                    alt={alt}
                    loader={<Preloader/>}/>
            </div>
            <div
                className="wrapper-text" style={textStyle}>
                <h3 className="header-cart">{title}</h3>
                <p className="description-cart">{description}</p>
            </div>
            </a>
        </Link>
    );
};

export default CartProject;