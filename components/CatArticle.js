import React from 'react';
import HeartIcon from "../svg/heart-icon.svg";
import ButtonShare from "./ButtonShare";
import Link from 'next/link';

const CatArticle = (props) => {

    const {article: {id, slug, date, title, pictureSrc, likeCount, categoryView}, index} = props;
    return (
        <div
            className="article"
            data-aos='fade-up'
            data-aos-delay={`${(index + 1) * 50}`}
            data-aos-offset="50"
        >
            <div className="inner-article">
                <Link as={`/blog/${slug}`} href={`/article?slug=${slug}`}>
                    <a className="content">
                        <div className="article__date">{date}</div>
                        <div className="article__title">{title}</div>
                        <div className="wrapper-img">
                            <img className="article__picture" src={pictureSrc} alt={title}/>
                        </div>
                    </a>
                </Link>
                <div className="article__additional-information">
                    <span className="count-like"><HeartIcon />{likeCount}</span>
                    <ButtonShare id={id}/>
                    <span className="category">{categoryView}</span>
                </div>
            </div>
        </div>
    );
};

export default CatArticle;