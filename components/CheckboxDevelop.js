import React from 'react';
import {Field} from "formik";

export const CheckboxDevelop = (props) => (
    <Field
        name={props.name}>
        {({ field, form }) => (
            <label className="label-checkbox">
                <input
                    className="input"
                    type="checkbox"
                    {...props}
                    checked={field.value.includes(props.value)}
                    onChange={() => {
                        if (field.value.includes(props.value)) {
                            const nextValue = field.value.filter(
                                value => value !== props.value
                            );
                            form.setFieldValue(props.name, nextValue);
                        } else {
                            const nextValue = field.value.concat(props.value);
                            form.setFieldValue(props.name, nextValue);
                        }
                    }}
                />
                <span className="square"> </span>
                {props.text}
            </label>
        )}
    </Field>
);