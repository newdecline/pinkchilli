import React, {useState, useEffect} from 'react';

const FilterPanel = props => {
    const [isFilterOpen, setFilterOpen] = useState(false);
    const [windowDimension, setWindowDimension] = useState(0);
    const {views, onChangeView, viewCart, categoriesPage, categorySelected, onChangeFilter} = props;

    const handleClickButtonSortItem = itemCategory => {
        setFilterOpen(!isFilterOpen);
        onChangeFilter(itemCategory);
        process.browser && window.scrollTo(0, 0);
    };

    const onResize = () => setWindowDimension(window.innerWidth);

    useEffect(() => {
        window.addEventListener('resize', onResize);
        onResize();
        return () => window.removeEventListener('resize', onResize);
    }, []);

    const classes = {
        filterPanel: ['filter-panel'],
    };

    isFilterOpen && windowDimension < 1599 ? classes.filterPanel.push('filter-panel_show') : null;

    return (
        <div className={isFilterOpen && windowDimension < 1599
            ? 'filter-panel-overlay'
            : null}>
            <div className="wrapper-btn-filter">
                <button className="btn-filter-mob" onClick={() => setFilterOpen(!isFilterOpen)}>
                    <span className="btn-filter-mob__item"> </span>
                    <span className="btn-filter-mob__item"> </span>
                    <span className="btn-filter-mob__item"> </span>
                </button>
            </div>

            <div className={classes.filterPanel.join(' ')}>
                {views
                    ?   <div className="btn-controls-view">
                        {
                            views.map((view) => {
                                return (
                                    <button
                                        onClick={() => onChangeView(view.view)}
                                        className={
                                            view.view === viewCart
                                                ? 'btn-view btn-view_active'
                                                : 'btn-view'
                                        }
                                        key={view.view}>
                                        {view.icon}
                                    </button>
                                );
                            })
                        }
                    </div>
                    : null}
                <div className="wrapper-sort">
                    {
                        categoriesPage.map((item, index) => {
                            return (
                                <button
                                    onClick={
                                        () => {handleClickButtonSortItem(item.category)}
                                    }
                                    style={{animationDuration: `0.5${10 * index}s`}}
                                    className={
                                        categorySelected === item.category
                                            ? 'button-sort-item button-sort-item_active'
                                            : 'button-sort-item'
                                    }
                                    key={item.label}>
                                    {item.label}
                                </button>
                            )}
                        )
                    }
                </div>
            </div>
        </div>
    );
};

export default FilterPanel;