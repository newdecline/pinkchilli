import React, {useState, useEffect, useRef} from 'react';
import {fetchApi} from '../services/api/fetchApi';
import {Field, Formik} from 'formik';
import * as Yup from 'yup';
import InputMask from 'react-input-mask';

import {AgreeCheckbox} from "./AgreeCheckbox";
import {CheckboxDevelop} from "./CheckboxDevelop";

const inputs = [
    {
        id: 'name',
        type: 'text',
        placeholder: 'Ваше имя *',
        errorMessage: 'Введите имя',
        values: 'name'
    },
    {
        id: 'phone',
        type: 'tel',
        placeholder: 'Ваш телефон *',
        errorMessage: 'Введите телефон',
        values: 'phone'
    },
    {
        id: 'email',
        type: 'email',
        placeholder: 'Ваша почта *',
        errorMessage: 'Введите почту',
        values: 'email'
    },
];

const FeedbackFormSchema = Yup.object().shape({
    email: Yup.string()
        .email()
        .required(),
    name: Yup.string()
        .min(2)
        .max(50)
        .required(),
    phone: Yup.string()
        .min(16)
        .max(50)
        .required(),
    agreement: Yup.bool().oneOf([true])
});

export const Form = props =>  {
    const textareaLabel = useRef(null);
    const [isFailedSendForm, setFailedForm] = useState(false);
    const [isSuccessSendForm, setSuccessSendForm] = useState(false);

    const scrollTopBeforeSubmit = () => {
        if (process.browser) {
            window.scrollTo(0, 0);
        }
    };

    useEffect(() => {
        textareaLabel.current && textareaLabel.current.querySelector('#message')
            .addEventListener('input', e => {
                if (e.data === null) {
                    e.target.style.height = "";
                    e.target.style.height = Math.min(e.target.scrollHeight) + "px";
                }
            })
    }, []);

    if (isFailedSendForm) {
        return  <h3 className="message-failed-send-form">Ошибка! Попробуйте позже.</h3>
    }

    if (isSuccessSendForm) {
        return <h6 className="success-message">{`Спасибо!`}<br/>{`Мы свяжемся с вами\nв ближайшее время.`}</h6>
    }

    return (
        <Formik
            initialValues={{
                checkboxList: [],
                name: '',
                phone: '',
                email: '',
                message: '',
                agreement: false,
            }}

            onSubmit={(values) => {
                fetchApi(`/contact`, {
                    method: 'POST',
                    body: JSON.stringify(values),
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    }
                })
                .then(() => {
                    scrollTopBeforeSubmit();
                    setSuccessSendForm(true);
                })
                .catch(() => {
                    setFailedForm(true);
                });
            }}

            validationSchema={FeedbackFormSchema}>

            {props => {
                const {
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleSubmit,
                } = props;

                return (
                    <form className="form" onSubmit={handleSubmit}>
                        <h3 className="section-title">Давайте обсудим ваш&nbsp;проект</h3>
                        <div className="form-top">
                            <span className="form-top__header">
                                Будем разрабатывать:
                                <span className="message-error">Выберите хотябы один раздел</span>
                            </span>
                            <div
                                className='wrapper-checkbox'>
                                <CheckboxDevelop
                                    onChange={handleChange}
                                    name="checkboxList"
                                    text="Сайт"
                                    value={1}
                                    />
                                <CheckboxDevelop
                                    onChange={handleChange}
                                    name="checkboxList"
                                    text="Бренд"
                                    value={2}
                                    />
                                <CheckboxDevelop
                                    onChange={handleChange}
                                    name="checkboxList"
                                    text="Логотип"
                                    value={3}/>
                                <CheckboxDevelop
                                    onChange={handleChange}
                                    name="checkboxList"
                                    text="Другое"
                                    value={4}
                                    />
                            </div>
                        </div>
                        <div className="wrapper-input">
                            {
                                inputs.map((input, index) => {
                                    if (input.type === 'tel') {
                                        return (
                                            <label
                                                htmlFor={input.name}
                                                key={index}
                                                className={errors[input.id] ?
                                                    'label-input label-input_error'
                                                    : 'label-input'}>
                                                <Field
                                                    name={input.id}
                                                    render={({field}) => (
                                                        <InputMask
                                                            {...field}
                                                            mask="+7 999 999 99 99"
                                                            maskChar=""
                                                            id={input.id}
                                                            type={input.type}
                                                            className="input"
                                                            placeholder="&nbsp;"
                                                            value={values[input.values]}
                                                        />
                                                    )}
                                                />
                                                <span className="placeholder">{input.placeholder}</span>
                                                <span className="bottom-line"> </span>
                                                <span className="error-message">{input.errorMessage}</span>
                                            </label>
                                        );
                                    }

                                    return (
                                        <label
                                            htmlFor={input.name}
                                            key={index}
                                            className={errors[input.id] ?
                                                'label-input label-input_error'
                                                : 'label-input'}>
                                            <Field
                                                component="input"
                                                id={input.id}
                                                type={input.type}
                                                className="input"
                                                placeholder="&nbsp;"
                                                value={values[input.values]}
                                                onChange={handleChange}
                                            />
                                            <span className="placeholder">{input.placeholder}</span>
                                            <span className="bottom-line"> </span>
                                            <span className="error-message">{input.errorMessage}</span>
                                        </label>
                                    );
                                })
                            }

                            <label
                                htmlFor="message"
                                className="label-input label-input-text-area"
                                ref={textareaLabel}>
                                <Field
                                    component="textarea"
                                    id="message"
                                    rows="1"
                                    className='input'
                                    placeholder="&nbsp;"
                                    value={values.message}
                                    onChange={handleChange}
                                />
                                <span className="placeholder">О проекте</span>
                                <span className="bottom-line"> </span>
                            </label>
                        </div>
                        <div className="bottom-form">
                            <Field
                                component={AgreeCheckbox}
                                name="agreement"
                                id="agreement"
                            />
                            <button
                                className="btn"
                                type="submit"
                                disabled={Object.keys(errors).length !== 0}>
                            </button>
                        </div>
                    </form>
                )}}
            </Formik>
    )
};
