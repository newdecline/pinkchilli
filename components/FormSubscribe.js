import React, {useState} from 'react';
import {fetchApi} from "../services/api/fetchApi";
import {Field, Formik} from "formik";
import * as Yup from "yup";
import {AgreeCheckbox} from "./AgreeCheckbox";

const inputs = [
    {
        id: 'name',
        type: 'text',
        placeholder: 'Ваше имя *',
        errorMessage: 'Введите имя',
        values: 'name'
    },
    {
        id: 'email',
        type: 'email',
        placeholder: 'Ваша почта *',
        errorMessage: 'Введите почту',
        values: 'email'
    },
];

const SubscribeFormSchema = Yup.object().shape({
    email: Yup.string()
        .email()
        .required(),
    name: Yup.string()
        .min(2)
        .max(50)
        .required(),
    agreement: Yup.bool().oneOf([true])
});

export const FormSubscribe = () => {
    const {isFailedSendForm, setFailedSendForm} = useState(false);
    const {isSuccessSendForm, setSuccessSendForm} = useState(false);

    return (
        <Formik
            initialValues={{
                name: '',
                email: '',
                agreement: false,
            }}
            onSubmit={(values) => {
                fetchApi(
                    '/subscribe',
                    {
                        method: 'POST',
                        body: JSON.stringify(values)
                    }
                ).then(() => {
                    setSuccessSendForm(true)
                }).catch(() => {
                    setFailedSendForm(true)
                });
            }}
            validationSchema={SubscribeFormSchema}>
            {props => {
                const {
                    values,
                    touched,
                    errors,
                    isSubmitting,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                } = props;
                {
                    if (isFailedSendForm) {
                        return <h3 className="message-failed-send-form">Ошибка! Попробуйте позже.</h3>
                    } else if (isSuccessSendForm) {
                        return  <h6 className="success-message">{`Подписка прошла успешно!`}</h6>
                    }
                }
            return (
                <form className="form-subscribe" onSubmit={handleSubmit}>
                    <h3 className="form-subscribe__title">Подпишитесь</h3>
                    <div className="form-subscribe__qualification">
                        Только полезные статьи один раз в неделю.
                    </div>
                    {inputs.map((input, index) => {
                        return (
                            <label
                                htmlFor={input.name}
                                key={index}
                                className={errors[input.id] && touched[input.id]
                                    ? 'label-input label-input_error'
                                    : 'label-input'}>
                                <Field
                                    component="input"
                                    id={input.id}
                                    type={input.type}
                                    className="input"
                                    placeholder="&nbsp;"
                                    value={values[input.values]}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                                <span className="placeholder">{input.placeholder}</span>
                                <span className="bottom-line"> </span>
                                <span className="error-message">{input.errorMessage}</span>
                            </label>
                        )
                    })}
                    <div className="form-subscribe__bottom">
                        <Field
                            component={AgreeCheckbox}
                            name="agreement"
                            id="agreement"
                        />
                        <button
                            className="btn form-subscribe__btn"
                            type="submit"
                            disabled={isSubmitting}>
                        </button>
                    </div>
                </form>
            )}}
        </Formik>
    );
};