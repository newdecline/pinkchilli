import React, {useEffect} from 'react';
import Link from 'next/link';
import {useRouter} from 'next/router';
import { useSelector, useDispatch  } from 'react-redux';
import {toggleMenu} from '../redux/actions/actionCreators';
import {changeViewport, setVhViewport} from '../services/viewport/changeViewport';
import LogoNoLetters from '../svg/logo-no-letters.svg';
import LogoMobile from '../svg/logo-mobile.svg';
import MailIcon from '../svg/mail-icon.svg';
import PhoneIcon from '../svg/phone-icon.svg';
import {BurgerMenu} from "./BurgerMenu";
import {SocialLinks} from "./SocialLinks";

const links = [
    {
        actualLink: '/projects',
        maskLink: '/proekty',
        label: 'Проекты'
    },
    /*{
        actualLink: '/about',
        maskLink: '/agenstvo',
        label: 'Агентство'
    },*/
    {
        actualLink: '/contacts',
        maskLink: '/kontakty',
        label: 'Контакты'
    },
    /*{
        actualLink: '/blog',
        maskLink: '/blog',
        label: 'Блог'
    }*/
];

export const Header = props => {
    const isOpenMenu = useSelector(state => state.menu.isOpenMenu);

    const dispatch = useDispatch();

    const router = useRouter();

    const setCollapsedHeader = () => {
        const body = document.body;

        const scrolled = window.pageYOffset || document.documentElement.scrollTop;

        if (scrolled > 160) {
            body.classList.add('collapse-header');
        } else {
            body.classList.remove('collapse-header');
        }
    };

    const classes = {
        header: ['header'],
    };

    isOpenMenu ? classes.header.push('header_open-menu') : null;
    router.asPath === '/' ? classes.header.push('header_index-page') : null;

    useEffect(() => {
        setVhViewport();

        changeViewport(setVhViewport);

        setCollapsedHeader();

        window.addEventListener('scroll', setCollapsedHeader);

        return () => {
            window.removeEventListener('scroll', setCollapsedHeader);
            window.removeEventListener("resize", setVhViewport);
        }
    }, []);

    return (
        <header
            className={classes.header.join(' ')}
        >
            <BurgerMenu />
            <Link as={'/'} href="/">
                <a
                    className='logo'
                    onClick={() => {
                        router.asPath !== '/' && isOpenMenu ? dispatch(toggleMenu()) : () => {}
                    }}>
                    <img
                        className="logo-big"
                        src='/logo-desktop.png'
                        alt="Креативное агентство Pink Chilli логотип"/>

                    {/*<LogoNoLetters/>*/}

                    <LogoNoLetters className="logo-collapse"/>

                    <LogoMobile className="logo-mobile"/>
                </a>
            </Link>
            <div className="wrapper-mobile-menu">
                <nav className="navigation">

                    {
                        links.map( (link, index) => (
                            <Link as={link.maskLink} href={link.actualLink} key={index}>
                                <a
                                    onClick={() => dispatch(toggleMenu())}
                                    className='link'>
                                    {link.label}
                                </a>
                            </Link>
                        ))
                    }
                </nav>
                <div className="wrapper-contacts">
                    <a className="contact-item" href="mailto:info@pinkchilli.ru">
                        <MailIcon className="contact-item__svg"/>
                        info@pinkchilli.ru
                    </a>

                    <a className="contact-item" href="tel:+79292371988">
                        <PhoneIcon className="contact-item__svg"/>
                        +7 929 237 19 88</a>
                </div>
                <SocialLinks />
            </div>
            <div className="wrapper-desktop-menu">
                <nav className="navigation">
                    {
                        links.map( (link, index) => (
                            <Link as={link.maskLink} href={link.actualLink} key={index}>
                                <a
                                    className={
                                        router.asPath === link.maskLink ?
                                            'link link_active' :
                                            'link'
                                    }>
                                    {link.label}
                                </a>
                            </Link>
                        ))
                    }
                </nav>
                <div className="wrapper-contacts">
                    <a className="contact-item" href="mailto:info@pinkchilli.ru">
                        <MailIcon className="contact-item__svg"/>
                        info@pinkchilli.ru
                    </a>

                    <a className="contact-item" href="tel:+79292371988">
                        <PhoneIcon className="contact-item__svg"/>
                        +7 929 237 19 88</a>
                </div>
            </div>
        </header>
    );
};