import React, { useState, useEffect, useRef } from 'react';
import Hammer from 'react-hammerjs';

export const MySlider = props => {
	const {
		startIndexSlide = 0,
		transitionSpeed = 1000,
		transitionMode = 'ease-in-out',
		slidesToShow = 1,
		beforeChangeSlide = () => {},
		renderPrevBtn = () => false,
		renderNextBtn = () => false,
		api = () => {}
	} = props;

	const slider = useRef(null);
	const [slideIndex, setSlideIndex] = useState(startIndexSlide || 0);
	const [leafWidth, setLeafWidth] = useState(0);
	const [animationMove, setAnimationMove] = useState(false);
	const [isSlideAlterable, setSlideAlterable] = useState(true);

	const handleChangeSlide = (e) => {
		if (isSlideAlterable) {
			if (e.deltaX > 0 && slideIndex !== 0 || e.target.closest('.slider__btn-prev')) {
				onChangeSlide(slideIndex - 1);
				beforeChangeSlide(slideIndex - 1);
				setSlideAlterable(false);
			} else if (e.deltaX < 0 && slideIndex + 1 !== props.children.length || e.target.closest('.slider__btn-next')) {
				if (slideIndex >= props.children.length - slidesToShow) {

				} else {
					onChangeSlide(slideIndex + 1);
					beforeChangeSlide(slideIndex + 1);
					setSlideAlterable(false);
				}
			}
		} else {
			return false;
		}
	};

	const handleResizeSlider = () => {
		setLeafWidth(slider.current.offsetWidth);
	};

	const onChangeSlide = (index = 0, withAnimation = true) => {
		setAnimationMove(withAnimation);
		setSlideIndex(index);
	};

	useEffect(() => {
		setLeafWidth(slider.current.offsetWidth / slidesToShow);
		window.addEventListener('resize', handleResizeSlider);
		return () => window.removeEventListener('resize', handleResizeSlider);
	}, []);

	api({ onChangeSlide });

	return (
		<Hammer
			style={{
				width: '100%',
				minHeight: '100%'
			}}
			onSwipe={handleChangeSlide}>
			<div>
				<div
					ref={slider}
					className='slider'>
					{renderPrevBtn() && <button
						disabled={slideIndex === 0}
						onClick={handleChangeSlide}
						className="slider__btn slider__btn-prev ">{renderPrevBtn(slideIndex)}</button>}

					<div
						className='slider__list'
						onTransitionEnd={() => setSlideAlterable(true)}
						style={{
							transform: `translate3d(-${slideIndex * leafWidth}px, 0px, 0px)`,
							transition: animationMove ? `transform ${transitionSpeed}ms ${transitionMode}` : `transform 0s ease-in-out`,
							width: props.children.length !== 0 ? props.children.length * leafWidth : leafWidth,
						}}
					>
						{React.Children.map(props.children, (child, index) => {
							return (
								<div
									className={index === slideIndex ?
										'slider__leaf slider__leaf_active' :
										'slider__leaf'}
									style={{
										width: `${leafWidth}px`
									}}
								>
									{child}
								</div>
							)
						})}
					</div>

					{renderNextBtn() && <button
						disabled={slideIndex + 1 === props.children.length}
						onClick={handleChangeSlide}
						className="slider__btn slider__btn-next ">{renderNextBtn(slideIndex)}</button>}
				</div>
			</div>
		</Hammer>
	)
};