import React from 'react'

export default () => (
    <div
        className='preloader'
    >
        <img src="/preloader.gif" alt='...load'/>
    </div>
)