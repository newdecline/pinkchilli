import React from 'react';

import InstagramIcon from '../svg/instagram-icon.svg';
import FacebookIcon from '../svg/facebook-icon.svg';
import VkIcon from '../svg/vk-icon.svg';
import BehanceIcon from '../svg/beehance-icon.svg';

export const SocialLinks = () => {
    return (
        <div className="social-links">
            <a
                rel="noreferrer"
                href="https://www.instagram.com/pinkchilli.agency/"
                target="_blank"
                className="icon-link">
                <InstagramIcon/>

            </a>
            <a
                rel="noreferrer"
                href="https://www.facebook.com/pinkchilli.agency/"
                target="_blank"
                className="icon-link">
                <FacebookIcon/>

            </a>
            <a
                rel="noreferrer"
                href="https://vk.com/pinkchilli.agency"
                target="_blank"
                className="icon-link">
                <VkIcon/>
            </a>
            <a
                rel="noreferrer"
                href="https://www.behance.net/pinkchilliagency"
                target="_blank"
                className="icon-link">
                <BehanceIcon/>
            </a>
        </div>
    );
};