import React, {useEffect} from "react";

export const Div100Vh = props => {
    const {children} = props;

    const setVh = () => {
        let vh = window.innerHeight * 0.01;
        document.documentElement.style.setProperty("--vh", `${vh}px`);
    };

    useEffect(() => {
        setVh();
        window.addEventListener("resize", setVh);

        return () => {
            window.removeEventListener("resize", setVh);
        }
    });

    return (
        <>
            {children}
        </>
    )
};

/* Use simple */
// top: calc(var(--vh, 1vh) * 90);