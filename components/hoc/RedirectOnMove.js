import React, {useEffect} from 'react';
import Swipe from 'react-easy-swipe';
import {useRouter} from "next/router";
import {useSelector} from 'react-redux';

export const RedirectOnMove = props => {
    const {as, href, children} = props;

    const isOpenMenu = useSelector(state => state.menu.isOpenMenu);

    const router = useRouter();

    const commonHandler = (e) => {
        if (!isOpenMenu) {
            if (e.keyCode === 40) {
                router.push(href, as, { shallow: true });
            }
            if (e.y < -40) {
                router.push(href, as, { shallow: true });
            }
            if (e.type === 'wheel') {
                router.push(href, as, { shallow: true });
            }
        }
    };

    useEffect(() => {
        window.addEventListener('keydown', commonHandler);
        window.addEventListener('wheel', commonHandler);

        return () => {
            window.removeEventListener('wheel', commonHandler);
            window.removeEventListener('keydown', commonHandler);
        }
    }, []);

    return (
        <Swipe
            onSwipeMove={(position) => commonHandler(position)}>
            {children}
        </Swipe>
    );
};