import React, { useEffect } from "react";
import {Header} from "../Header";
import Head from "next/head";
import {useRouter } from 'next/router';

export const MainLayout = props => {
    const {children, className, customCode} = props;

    const router = useRouter();

    const handleRouteChange = url => {
        if (history.state.as !== url && process.env.NODE_ENV === 'production') {
            // get(window, Ya.Metrika2.counters()[0].id, null) && ym(+window.Ya.Metrika2.counters()[0].id, 'hit', url);
            // get(window, ga, null) && ga('send', 'pageview', url);
            // TODO проверить как работает метрика
            // ym && typeof ym === 'function' && ym(+window.Ya.Metrika2.counters()[0].id, 'hit', url);
            // ga && typeof ga === 'function' && ga('send', 'pageview', url);
        }
    };

    useEffect(() => {
        router.events.on('beforeHistoryChange', handleRouteChange);
        return () => {
            router.events.off('beforeHistoryChange', handleRouteChange);
        }
    }, []);

    return (
        <div className={className}>
            <Head>
                <meta name="viewport" content="initial-scale=1.0, width=device-width"/>
                <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
                <link
                    href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700&amp;subset=cyrillic"
                    rel="stylesheet" />
                <meta name="yandex-verification" content="49af3fe00caf60f3" />
                {/*{customCode.head && <script type="text/javascript" dangerouslySetInnerHTML = {{__html: customCode.head}} />}*/}
            </Head>
            <Header />
            {children}
        </div>
    )
};