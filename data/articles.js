const articles = [
    {
        id: 1,
        slug: '10-sekretov-razrabotchika',
        date: '1 апреля 2019',
        title: 'Web и IT',
        pictureSrc: '/articles-picture/001.jpg',
        likeCount: 238,
        category: 'webIT',
        categoryView: 'Web и IT'
    },
    {
        id: 2,
        slug: '10-sekretov-dizaina',
        date: '2 апреля 2019',
        title: 'Брендинг и дизайн',
        pictureSrc: '/articles-picture/002.jpg',
        likeCount: 26,
        category: 'brandingDesign',
        categoryView: 'Брендинг и дизайн'
    },
    {
        id: 3,
        slug: '10-sekretov-biznessa',
        date: '3 апреля 2019',
        title: 'Развитие бизнеса',
        pictureSrc: '/articles-picture/003.jpg',
        likeCount: 124,
        category: 'businessDevelopment',
        categoryView: 'Развитие бизнеса'
    },
    {
        id: 4,
        slug: '10-sekretov-interviu',
        date: '4 апреля 2019',
        title: 'Интервью',
        pictureSrc: '/articles-picture/004.jpg',
        likeCount: 422,
        category: 'interview',
        categoryView: 'Интервью'
    },
    {
        id: 5,
        slug: '10-sekretov-cheklista',
        date: '5 апреля 2019',
        title: 'Планеры и чек-листы',
        pictureSrc: '/articles-picture/005.jpg',
        likeCount: 452,
        category: 'glidersChecklists',
        categoryView: 'Планеры и чек-листы'
    },
    {
        id: 6,
        slug: '10-sekretov-shablona',
        date: '6 апреля 2019',
        title: 'Шаблоны для Instagram салона красоты',
        pictureSrc: '/articles-picture/006.jpg',
        likeCount: 12,
        category: 'freeTemplates',
        categoryView: 'Бесплатные шаблоны'
    },
];

export default articles;