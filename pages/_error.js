import React from 'react';
import Link from 'next/link';
import {withRedux} from "../redux/withRedux";

const Error = props => {
    return (
        <div className="error-contents">
            <div className="hr"> </div>
            <h6 className="error-title">{props.statusCode}</h6>
            <p className="error-subtitle">
                {`Упс...\nПохоже что-то пошло не так.\nНе расстраивайтесь,\nпосмотрите один из наших\n`}<Link as={'/proekty'} href={'/projects'}>проектов!</Link>
            </p>
            <div className="copywriter">{"Все права защищены ИП Осипова А.А.\n2019"}</div>
        </div>
    );
};

Error.getInitialProps = ({ res, err }) => {
    const statusCode = res ? res.statusCode : err ? err.statusCode : 404;

    return { statusCode};
};

export default withRedux(Error, {classes: ['page-error'], fetchURLLayout: null});