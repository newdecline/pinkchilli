import React, {Component} from 'react';
import TimeIcon from '../svg/time-icon.svg';
import Link from 'next/link';
import { withRouter } from 'next/router';

import HeartIcon from "../svg/heart-icon.svg";
import ArrowLeftIcon from "../svg/left-arrow.svg";
import ButtonShare from "../components/ButtonShare";
import {FormSubscribe} from "../components/FormSubscribe";
import Preloader from "../components/Preloader";

class Article extends Component {
    state = {
        isMountedArticle: false,
        slug: null
    };

    componentDidMount() {
        this.setState(() => ({
            isMountedArticle: true,
            slug: this.props.router.query.slug
        }));
        window.scrollTo(0, 0);
    }

    render() {
        const {slug} = this.state;

        if (!slug) {
            return <Preloader />
        }

        return (
            <>
                <div className="hr"> </div>
                <div className="page-article">
                    <div className="date">6 апреля 2019</div>
                    <h3 className="title">
                        {`Статья под номером ${slug}`}
                    </h3>
                    <div className="additional-information-top">
                        <span className="time"><TimeIcon />10 минут</span>
                        <span className="category">Брендинг и дизайн</span>
                    </div>
                    <p className="paragraph">
                        Хотя фраза и бессмысленна, она имеет давнюю историю.
                        Фраза использовалась печатниками многие столетия для демонстрации наиболее важных особенностей своих шрифтов.
                        Она использовалась потому, что символы составляют сложные по межсимвольным
                        промежуткам и по комбинациям символов пары, наилучшим образом демонстрирующие преимущества данного начертания.
                    </p>
                    <p className="paragraph">
                        В своей статье от 1994-го года журнал «Before & After» отследил фразу «Lorem ipsum ...»
                        до философского трактата Цицерона О пределах добра и зла, написанного в 45 году до нашей
                        эры на латинском языке. В оригинале текст выглядит так «Neque porro quisquam est qui dolorem ipsum quia
                        dolor sit amet, consectetur, adipisci velit ...», и переводится как «Нет никого, кто любил бы свою боль,
                        кто искал бы ее и хотел бы чтобы она была у него. Потому что это боль...»
                    </p>
                    <p className="paragraph">
                        В шестнадцатом веке печатники постепенно преобразовали текст
                        Цицерона в представленый пример. С того времени этот, похожий на латинский,
                        текст стал стандартом в печатной промышленности для примеров шрифтов и текстов.
                        Перед появлением электронных издательств дизайнеры импровизировали в работе над макетами,
                        изображая текст при помощи волнистых линий.
                        С появлением самоклеющихся наклеек с напечатанным текстом «Lorem ipsum» появился
                        более реалистичный способ обозначения расположения текста на странице..
                    </p>
                    <img src="/public/articles-picture/001.jpg" alt="statya"/>
                    <h4>
                        Какой-нибудь подзаголовок
                    </h4>
                    <p className="paragraph">
                        Хотя фраза и бессмысленна, она имеет давнюю историю.
                        Фраза использовалась печатниками многие столетия для демонстрации наиболее важных особенностей своих шрифтов.
                        Она использовалась потому, что символы составляют сложные по межсимвольным
                        промежуткам и по комбинациям символов пары, наилучшим образом демонстрирующие преимущества данного начертания.
                    </p>
                    <p className="paragraph">
                        В своей статье от 1994-го года журнал «Before & After» отследил фразу «Lorem ipsum ...»
                        до философского трактата Цицерона О пределах добра и зла, написанного в 45 году до нашей
                        эры на латинском языке. В оригинале текст выглядит так «Neque porro quisquam est qui dolorem ipsum quia
                        dolor sit amet, consectetur, adipisci velit ...», и переводится как «Нет никого, кто любил бы свою боль,
                        кто искал бы ее и хотел бы чтобы она была у него. Потому что это боль...»
                    </p>
                    <h4>
                        Какой-нибудь подзаголовок
                    </h4>
                    <p className="paragraph">
                        Хотя фраза и бессмысленна, она имеет давнюю историю.
                        Фраза использовалась печатниками многие столетия для демонстрации наиболее важных особенностей своих шрифтов.
                        Она использовалась потому, что символы составляют сложные по межсимвольным
                        промежуткам и по комбинациям символов пары, наилучшим образом демонстрирующие преимущества данного начертания.
                    </p>
                    <p className="paragraph">
                        В своей статье от 1994-го года журнал «Before & After» отследил фразу «Lorem ipsum ...»
                        до философского трактата Цицерона О пределах добра и зла, написанного в 45 году до нашей
                        эры на латинском языке. В оригинале текст выглядит так «Neque porro quisquam est qui dolorem ipsum quia
                        dolor sit amet, consectetur, adipisci velit ...», и переводится как «Нет никого, кто любил бы свою боль,
                        кто искал бы ее и хотел бы чтобы она была у него. Потому что это боль...»
                    </p>
                    <p className="paragraph">
                        В шестнадцатом веке печатники постепенно преобразовали текст
                        Цицерона в представленый пример. С того времени этот, похожий на латинский,
                        текст стал стандартом в печатной промышленности для примеров шрифтов и текстов.
                        Перед появлением электронных издательств дизайнеры импровизировали в работе над макетами,
                        изображая текст при помощи волнистых линий.
                        С появлением самоклеющихся наклеек с напечатанным текстом «Lorem ipsum» появился
                        более реалистичный способ обозначения расположения текста на странице..
                    </p>
                    <div className="wrapper">
                        <a download href="/public/articles-picture/001.jpg" className="download">СКАЧАТЬ ЧЕК-ЛИСТ</a>
                        <div className="additional-information-bottom">
                            <Link as={`/blog`} href={`/blog`}>
                                <a className="all-articles"><ArrowLeftIcon />Все статьи</a>
                            </Link>
                            <span className="count-like"><HeartIcon />12</span>
                            <ButtonShare />
                        </div>
                    </div>
                    <FormSubscribe />
                    <div className="copywriter_mob">Все права защищены PinkChilli Agency 2019</div>
                    <div className="copywriter_desk">Все права защищены PinkChilli Agency 2019</div>
                </div>
            </>
        );
    }
}

export default withRouter(Article);