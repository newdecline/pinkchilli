import React, {Component} from "react";
const imagesLoaded = require("imagesloaded");
import AOS from 'aos';
import { Watch } from 'scrollmonitor-react';
import Router from 'next/router';

import articles from "../data/articles";
import {FormSubscribe} from "../components/FormSubscribe";
import CatArticle from "../components/CatArticle";
import FilterPanel from "../components/FilterPanel";

const categoriesPage = [
    {category: 'all', label: 'Все статьи'},
    {category: 'webIT', label: 'Web и IT'},
    {category: 'brandingDesign', label: 'Брендинг и дизайн'},
    {category: 'businessDevelopment', label: 'Развитие бизнеса'},
    {category: 'interview', label: 'Интервью'},
    {category: 'glidersChecklists', label: 'Планеры и чек-листы'},
    {category: 'freeTemplates', label: 'Бесплатные шаблоны'}
];

export default Watch(
    class Blog extends Component {
        state = {
            categorySelected: 'all',
            isFilterOpen: false
        };

        onChangeFilter = (nameFilter) => {
            this.setState({
                categorySelected: nameFilter
            });

            this.handlerRouterPush(nameFilter)
        };

        onToggleOpenFilter = () => {
            this.setState(state => {
                return {
                    isFilterOpen: !state.isFilterOpen
                }
            });
        };

        handlerRouterPush = (nameFilter = 'all') => {
            Router
                .push(`/blog?category=${nameFilter}`,
                    `/blog?category=${nameFilter}`,
                    {shallow: true}
                );
        };

        updateArticlesGrid = () => {
            function resizeGridItem(item) {
                const grid = document.getElementsByClassName("container-articles")[0];
                const rowHeight = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-auto-rows'));
                const rowGap = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-row-gap'));
                const rowSpan = Math.ceil((item.querySelector('.inner-article').getBoundingClientRect().height + rowGap) / (rowHeight + rowGap));
                item.style.gridRowEnd = "span " + rowSpan;
            }

            function resizeAllGridItems() {
                const allItems = document.getElementsByClassName("article");
                for (let x = 0; x < allItems.length; x++) {
                    resizeGridItem(allItems[x]);
                }
            }

            window.onload = resizeAllGridItems;

            window.addEventListener("resize", resizeAllGridItems);

            function resizeInstance(instance) {
                const item = instance.elements[0];
                resizeGridItem(item);
            }

            const allItems = document.getElementsByClassName("article");

            for (let x = 0; x < allItems.length; x++) {
                imagesLoaded(allItems[x], resizeInstance);
            }
        };

        componentDidUpdate(prevProps, prevState, snapshot) {
            if (prevState.categorySelected !== this.state.categorySelected) {
                this.updateArticlesGrid();
            }
            this.aos.refresh();
        }

        componentDidMount() {
            this.updateArticlesGrid();
            this.aos = AOS;
            this.aos.init({
                duration: 700,
                offset: 10
            });
            this.setState(() => ({categorySelected: Router.router.query.category || 'all'}));
        }

        render() {
            const {categorySelected} = this.state;

            return (
                <>
                    <div className="hr"> </div>
                    <div className="blog-page">
                        <FilterPanel
                            onChangeFilter={this.onChangeFilter}
                            categorySelected={categorySelected}
                            categoriesPage={categoriesPage}/>
                        <div className="wrapper-content">
                            <div className="intro">
                                <h2 className="intro__title">Блог Pink Chilli Agency</h2>
                                <p className="intro__description">
                                    Полезные статьи о разработке сайтов, брендинге, дизайне
                                    и маркетинге.
                                </p>
                                <p className="intro__description">
                                    Интервью с экспертами отрасли.
                                </p>
                                <p className="intro__description">
                                    Инструменты для развития бизнеса, планеры, чек-листы и бесплатные
                                    шаблоны для дизайна социальных сетей.
                                </p>
                            </div>
                            <FormSubscribe/>
                            <div className="container-articles">
                                {articles.filter((article) => {
                                    if (categorySelected === 'all') {
                                        return articles
                                    }
                                    return (
                                        article.category === categorySelected
                                    );
                                })
                                    .map((article, index) => {
                                        return (
                                            <CatArticle key={article.id} article={article} index={index}/>
                                        );
                                    })}
                            </div>
                        </div>
                        <div className="copywriter_mob">Все права защищены PinkChilli Agency 2019</div>
                        <div className="copywriter_desk">Все права защищены &nbsp;&nbsp;PinkChilli Agency&nbsp;&nbsp; 2019</div>
                    </div>
                </>
            );
        }
    }
);