import React from 'react';
import Head from 'next/head';
import {Form} from "../components/Form";
import {SocialLinks} from "../components/SocialLinks";
import {withRedux} from "../redux/withRedux";

 const Contacts = props => {

    return (
        <>
            <Head>
                <title>Создание и разработка сайтов под ключ – Напишите нам, и мы обсудим ваш проект</title>
                <meta
                    name="description"
                    content="Разработка уникальных сайтов, логотипов, фирменного стиля компании. Оставьте заявку, и мы с вами свяжемся."/>
            </Head>

            <div className="hr"> </div>
            <div className="wrapper">
                <div className="left-col">
                    <Form/>
                    <div className="copywriter_desk">Все права защищены &nbsp;&nbsp;Pink Chilli Agency&nbsp;&nbsp; 2019</div>
                </div>
                <div className="right-col">
                    <div className="wrapper-address">
                        <div className="building">SVOBODA2</div>
                        <div className="address">{"Челябинск,\nул. Свободы 2, к.5\nофис 3.16"}</div>
                    </div>
                    <a href="mailto:info@pinkchilli.ru" className="mail">info@pinkchilli.ru</a>
                    <a href="tel:+79292371988" className="tel">+7 929 237 19 88</a>
                    <SocialLinks />
                    <div className="copywriter_mob">Все права защищены Pink&nbsp;Chilli&nbsp;Agency {new Date().getFullYear()}</div>
                </div>
                {/*<div className="map-col">
                <div className="collapse-btn"><ArrowLeftIcon/></div>
            </div>*/}
            </div>
        </>
    )
};

export default withRedux(Contacts, {classes: ['page-contacts'], fetchURLLayout: null});