import React from "react";
import Head from 'next/head';
import {SocialLinks} from "../components/SocialLinks";
import {withRedux} from "../redux/withRedux";
import {RedirectOnMove} from "../components/hoc/RedirectOnMove";

const Index = props => {

    return (
        <RedirectOnMove as={'/proekty'} href={'/projects'}>
            <Head>
                <title>Создание и разработка сайтов под ключ – Креативное агентство Pink Chilli'</title>
                <meta
                    name="description"
                    content='Разработка уникальных сайтов с нуля под ключ, создание логотипа и фирменного стиля.
                    Вдумчивый дизайн, индивидуальный подход к каждому проекту!
                    С нами ваш бизнес не останется незамеченным.'/>
            </Head>

            <div className="container">
                <div className="wrapper-text">
                    <h1 className="title-header">{"Мы создаем сайты, айдентику \nи бренды с характером"}</h1>
                </div>
                <div className="wrapper-text">
                    <h2 className="title-header_sub">{"Эффектный дизайн \nи эффективные решения для бизнеса"}</h2>
                </div>
                <div className="wrapper-statistics">
                    <div className="statistic-item">
                        <div className="statistic-item__count">9</div>
                        <div className="statistic-item__text">лет <br/> в индустрии</div>
                    </div>
                    <div className="statistic-item">
                        <div className="statistic-item__count">218</div>
                        <div className="statistic-item__text">реализованных <br/> проектов</div>
                    </div>
                </div>
                <SocialLinks />
            </div>
        </RedirectOnMove>
    );
};

export default withRedux(Index, {classes: ['page-index'], fetchURLLayout: null});