import React, {useState, useEffect} from 'react';
import Link from 'next/link';
import Head from 'next/head';
import {Collapse} from 'react-collapse';
import Img from 'react-image';
import {Form} from '../components/Form';
import {projects} from '../data/projects';
import PrevArrowIcon from '../svg/left-arrow.svg'
import NextArrowIcon from '../svg/right-arrow.svg'
import InfoIcon from '../svg/info-icon.svg';
import Preloader from '../components/Preloader';
import {withRedux} from "../redux/withRedux";

const Project = props => {
  const {project, currentIndex, projectsCount} = props;

  const [isHideInfoProject, setHideInfoProject] = useState(false);

  const [device, setDevice] = useState('desktop');

  const handleResize = () => window.innerWidth >= 1600 ? setDevice('desktop') : setDevice('mobile');

  useEffect(() => {
    window.addEventListener('resize', handleResize);
    handleResize();
    return () => window.removeEventListener('resize', handleResize);
  }, []);

  if (!project) {
    const err = new Error();
    err.statusCode = 404;
    err.code = 'ENOENT';
    throw err;
  }

  const {fullImg, alt, client, madeBy, description, madeDetailList, year} = project;

  const onToggleMoreInfo = () => {
    setHideInfoProject(!isHideInfoProject);
  };

  const handleClickBtnProject = () => {
    setHideInfoProject(false)
  };

  const classes = {
    pageProject: ['page-project'],
    wrapperBtn: ['wrapper-btn']
  };

  currentIndex === 0 ? classes.wrapperBtn.push('wrapper-btn_first-project') : null;

  return (
    <main key={props.slug}>
      <Head key={props.slug}>
        <title>Создание и разработка сайтов под ключ – Примеры работ – {alt}</title>
        <meta
          name="description"
          content={`Разработка уникальных сайтов, логотипов, фирменного стиля компании.
                    Индивидуальный подход к каждому проекту, вдумчивый дизайн!
                    Проект – ${alt}`}/>
      </Head>

      <div className="hr"/>
      {
        device === 'desktop'
          ? desktopContent(description, client, madeBy, onToggleMoreInfo, madeDetailList, year)
          : mobileContent(description, client, madeBy, onToggleMoreInfo, madeDetailList, isHideInfoProject, year)
      }

      <div className="wrapper-img-project">

        {
          fullImg.map((img, index) => {
            return (
              <Img
                className="project-img-full"
                src={img.src}
                alt={img.alt}
                key={index}
                loader={<Preloader/>}/>
            )
          })
        }

        <div className={classes.wrapperBtn.join(' ')}>
          {
            currentIndex !== 0 ?
              <Link
                as={`/proekty/${projects[currentIndex - 1].slug}`}
                href={`/project?slug=${projects[currentIndex - 1].slug}`}>
                <a className="btn-project btn-prev" onClick={handleClickBtnProject}>
                  <PrevArrowIcon/>Предыдущий
                </a>
              </Link>
              : null
          }

          {
            projectsCount - 1 !== currentIndex ?
              <Link
                as={`/proekty/${projects[currentIndex + 1].slug}`}
                href={`/project?slug=${projects[currentIndex + 1].slug}`}>
                <a className="btn-project btn-next" onClick={handleClickBtnProject}>
                  Следующий<NextArrowIcon/>
                </a>
              </Link>
              : null
          }
        </div>
      </div>

      <div className="wrapper-form-project">
        <Form/>
        <div className="copywriter_mob">Все права защищены Pink&nbsp;Chilli&nbsp;Agency 2019</div>
        <div className="copywriter_desk">Все права защищены &nbsp;&nbsp;Pink&nbsp;Chilli&nbsp;Agency&nbsp;&nbsp; 2019
        </div>
      </div>

    </main>
  );
};

function desktopContent(description, client, madeBy, onToggleMoreInfo, madeDetailList, year) {
  return (
    <div className="desktop-content">
      <div className="short-info">
        <h6 className="project-title">
          {description}
        </h6>
        <div className="client">Клиент:</div>
        <div className="client-name">{client}</div>
        <div className="made-title">Сделано:</div>
        <ul className="made-list">
          {madeBy.map((item, i) => <li key={i}>{item}</li>)}
        </ul>
        <div className="date">{year}</div>
      </div>

      <div className="info">
        <h6 className="project-title">
          {description}
        </h6>
        <ul className="made-detail-list">
          {madeDetailList.map((item, i) => <li key={i}>{typeof item === 'function' ? item() : item}</li>)}
        </ul>
      </div>
    </div>
  )
}

function mobileContent(description, client, madeBy, onToggleMoreInfo, madeDetailList, isHideInfoProject, year) {
  return (
    <div className="mobile-content">
      <h6 className="project-title">
        {description}
        <button className="btn-info-more" onClick={() => onToggleMoreInfo()}>
          <span className="btn-info-more__icon"><InfoIcon/>О проекте</span>
        </button>
      </h6>
      <Collapse isOpened={isHideInfoProject} hasNestedCollapse={true}>
        <div className="short-info">
          <div className="client">Клиент:</div>
          <div className="client-name">{client}</div>
          <div className="made-title">Сделано:</div>
          <ul className="made-list">
            {madeBy.map((item, i) => <li key={i}>{item}</li>)}
          </ul>
          <div className="date">{year}</div>
        </div>

        <div className="info">
          <ul className="made-detail-list">
            {madeDetailList.map((item, i) => <li key={i}>{typeof item === 'function' ? item() : item}</li>)}
          </ul>
        </div>
      </Collapse>
    </div>
  )
}

Project.getInitialProps = async ({query, res}) => {
  const { slug } = query;

  const project = projects.find(project => project.slug === slug);
  const currentIndex = projects.findIndex(project => project.slug === slug);
  const projectsCount = projects.length;

  if (!project) {
    if (res) {
      res.statusCode = 404;
    }

    return {
      err: {
        statusCode: 404
      }
    };
  }

  return {project, currentIndex, projectsCount}
};

export default withRedux(Project, {fetchURLLayout: null, classes: ['page-project']});