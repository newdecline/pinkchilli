import React, {useState, useEffect} from 'react';
import AOS from 'aos';
import Router from 'next/router';
import Head from 'next/head';
import CartProject from "../components/CartProject";
import FilterTileIcon from '../svg/filter-tile-icon.svg';
import FilterCartIcon from '../svg/filter-cart-icon.svg';
import {projects as initProjects} from '../data/projects';
import FilterPanel from "../components/FilterPanel";
import {MySlider} from "../components/MySlider";

import {withRedux} from "../redux/withRedux";

const categoriesPage = [
  {category: 'all', label: 'Все проекты'},
  {category: 'branding', label: 'Брендинг'},
  // {category: 'advertisingСompanies', label: 'Рекламные кампании'},
  // {category: 'creativity', label: 'Креатив'},
  {category: 'sites', label: 'Сайты'}
];

const views = [
  {
    view: 'list',
    icon: <FilterTileIcon/>
  },
  {
    view: 'tile',
    icon: <FilterCartIcon/>
  }
];

const Projects = props => {
  let mySlider;

  const [projects, setProjects] = useState(initProjects);
  const [filter, setFilter] = useState({
    categorySelected: 'all',
    viewCart: 'list'
  });

  const onChangeView = view => {
    setFilter({
      ...filter,
      viewCart: view
    });
    handlerRouterPush(view, filter.categorySelected);
  };

  const onChangeFilter = nameFilter => {
    if (filter.categorySelected !== nameFilter) {
      setFilter({
        ...filter,
        categorySelected: nameFilter
      });
      handlerRouterPush(filter.viewCart, nameFilter);
      filter.viewCart === 'tile' && mySlider.onChangeSlide(0, false);
    }
  };

  const handlerScrollTo = () => {
    if (process.browser) {
      window.scrollTo(0, 0);
    }
  };

  const handlerRouterPush = (view = 'list', nameFilter = 'all', slideIndex = 0) => {
    Router.push(
      `/projects?view=${view}&category=${nameFilter}&slideIndex=${slideIndex}`,
      `/proekty?view=${view}&category=${nameFilter}&slideIndex=${slideIndex}`,
      {shallow: true}
    );
  };

  useEffect(() => {
    AOS.refresh();
  }, [filter.viewCart]);

  useEffect(() => {
    AOS.init();
    setFilter({
      viewCart: Router.router.query.view || 'list',
      categorySelected: Router.router.query.category || 'all',
    });
  }, []);

  const cartList = projects.filter((project) => {
    if (filter.categorySelected === 'all') {
      return initProjects
    }
    return (
      project.category === filter.categorySelected
    );
  }).map((project, index) => {
    return (
      <CartProject
        project={project}
        index={index}
        key={project.slug}
        viewCart={filter.viewCart}
      />
    );
  });

  const colorsButton = cartList.map(item => {
    return item.props.project.textColor;
  });

  return (
    <>
      <Head>
        <title>Создание и разработка сайтов под ключ -Примеры работ</title>
        <meta
          name="description"
          content="Разработка уникальных сайтов, логотипов, фирменного стиля компании.
                    Индивидуальный подход к каждому проекту, вдумчивый дизайн!
                    Примеры наших работ."/>
      </Head>

      <div className="hr"></div>

      <div className="content-projects">
        <FilterPanel
          onChangeFilter={onChangeFilter}
          onChangeView={onChangeView}
          isFilterOpen={filter.isFilterOpen}
          categorySelected={filter.categorySelected}
          viewCart={filter.viewCart}
          categoriesPage={categoriesPage}
          views={views}/>
        <div
          onAnimationStart={handlerScrollTo}
          className={
            filter.viewCart === 'tile' ?
              'wrapper-cart-project wrapper-cart-project_tile' :
              'wrapper-cart-project'
          }>
          {
            filter.viewCart === 'list' ? cartList :
              <MySlider
                api={api => mySlider = api}
                startIndexSlide={+props.slideIndex}
                transitionSpeed={800}
                beforeChangeSlide={(slideIndex) => {
                  handlerRouterPush(filter.viewCart, filter.categorySelected, slideIndex)
                }}
                renderPrevBtn={(slideIndex) => (
                  <>
                                    <span>
                                        <svg
                                          width="7"
                                          height="12"
                                          viewBox="0 0 7 12"
                                          fill="none">
                                            <path d="M6 11L1 6L6 1" stroke={colorsButton[slideIndex]}/>
                                        </svg>
                                    </span>
                    <span style={{color: colorsButton[slideIndex]}}>Предыдущий</span>
                  </>)}
                renderNextBtn={(slideIndex) => (
                  <>
                    <span style={{color: colorsButton[slideIndex]}}>Следующий</span>
                    <span>
                                        <svg
                                          width="7"
                                          height="12"
                                          viewBox="0 0 7 12"
                                          fill="none">
                                            <path d="M6 11L1 6L6 1" stroke={colorsButton[slideIndex]}/>
                                        </svg>
                                    </span>
                  </>)}
              >
                {
                  cartList
                }
              </MySlider>
          }
        </div>
      </div>
    </>
  );
};

Projects.getInitialProps = async ({query}) => {
  return query
};

export default withRedux(Projects, {fetchURLLayout: null, classes: ['page-projects']});