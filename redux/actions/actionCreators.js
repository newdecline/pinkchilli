import {actionTypes} from "../actions/actionTypes";

export const toggleMenu = () => {
    return { type: actionTypes.TOGGLE_MENU }
};