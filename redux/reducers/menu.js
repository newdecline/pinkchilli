import {actionTypes} from "../actions/actionTypes";

const initialState = {
    isOpenMenu: false
};

export const menu = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.TOGGLE_MENU:
            return {...state, isOpenMenu: !state.isOpenMenu};
        default:
            return state
    }
};