import React from 'react';
import App from 'next/app';
import { Provider } from 'react-redux';
import { initializeStore } from './store'
import {MainLayout} from "../components/layouts/MainLayout";
import {fetchApi} from "../services/api/fetchApi";
import {Div100Vh} from "../components/hoc/Div100Vh";

import '../sass/styles.scss';

export const withRedux = (
    PageComponent, { ssr = true, Layout = MainLayout, fetchURLLayout = '/layouts', classes = []} = {}) => {

    const WithRedux = ({ initialReduxState, ...props }) => {

        const store = getOrInitializeStore(initialReduxState);

        return (
            <Provider store={store}>
                <Layout {...props.data} className={classes.join(' ')}>
                    <Div100Vh>
                        <PageComponent {...props}/>
                    </Div100Vh>
                </Layout>
            </Provider>
        )
    };

    if (process.env.NODE_ENV !== 'production') {
        const isAppHoc =
            PageComponent === App || PageComponent.prototype instanceof App;
        if (isAppHoc) {
            throw new Error('The withRedux HOC only works with PageComponents')
        }
    }

    if (process.env.NODE_ENV !== 'production') {
        const displayName =
            PageComponent.displayName || PageComponent.name || 'Component';

        WithRedux.displayName = `withRedux(${displayName})`
    }

    if (ssr || PageComponent.getInitialProps) {
        WithRedux.getInitialProps = async context => {
            const reduxStore = getOrInitializeStore();

            context.reduxStore = reduxStore;

            let pageProps =
                typeof PageComponent.getInitialProps === 'function'
                    ? await PageComponent.getInitialProps(context)
                    : {};

            if (fetchURLLayout) {
                return fetchApi(`${fetchURLLayout}`).then(res => ({
                        ...pageProps,
                        data: res,
                        initialReduxState: reduxStore.getState()
                    })
                );
            } else {
                return {
                    ...pageProps,
                    initialReduxState: reduxStore.getState()
                }
            }
        }
    }

    return WithRedux
};

let reduxStore;

const getOrInitializeStore = initialState => {
    if (typeof window === 'undefined') {
        return initializeStore(initialState)
    }

    if (!reduxStore) {
        reduxStore = initializeStore(initialState)
    }

    return reduxStore
};