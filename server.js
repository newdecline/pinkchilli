const express = require('express');
const next = require('next');
const proxy = require('http-proxy-middleware');

const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();
const port = parseInt(process.env.PORT, 10) || 3000;

const optionsProxy = proxy({
    target: process.env.BASE_URL,
    changeOrigin: true
});

app
    .prepare()
    .then(() => {
        const server = express();

        server.use(['/api'], optionsProxy);

        server.get('/', (req, res) => {
            const actualPage = '/index';
            app.render(req, res, actualPage);
        });

        server.get('/proekty', (req, res) => {
            const actualPage = '/projects';
            const queryParams = req.query;
            app.render(req, res, actualPage, queryParams);
        });

        server.get('/proekty/:slug', (req, res) => {
            const actualPage = '/project';
            const queryParams = { slug: req.params.slug };
            app.render(req, res, actualPage, queryParams);
        });

        // server.get('/agenstvo', (req, res) => {
        //     const actualPage = '/about';
        //     app.render(req, res, actualPage);
        // });

        server.get('/kontakty', (req, res) => {
            const actualPage = '/contacts';
            app.render(req, res, actualPage);
        });

        // server.get('/blog', (req, res) => {
        //     const actualPage = '/blog';
        //     const queryParams = req.query;
        //     app.render(req, res, actualPage, queryParams);
        // });
        //
        // server.get('/blog/:slug', (req, res) => {
        //     const actualPage = '/article';
        //     const queryParams = { slug: req.params.slug };
        //     app.render(req, res, actualPage, queryParams);
        // });

        server.get('*', (req, res) => {
            return handle(req, res)
        });

        const listenCallback = err => {
            if (err) throw err;
            console.log(`> Ready on http://localhost:${port}`)
        };

        dev ? server.listen(port, listenCallback) : server.listen(port, 'localhost', listenCallback);
    })
    .catch(ex => {
        console.error(ex.stack);
        process.exit(1);
    });