import fetch from 'isomorphic-unfetch';

let baseUrl = '';

if (process.browser) {
    baseUrl = `${window.location.origin}/api`;
} else {
    baseUrl = `${process.env.BASE_URL}/api`;
}

export const fetchApi = async (url, init = {}) => {
    const result = await fetch(`${baseUrl}${url}`,{...init});

    if (!result.ok) {
        throw new Error(`Ошибка при запросе ${url}, код ${result.status}`);
    }

    return await result.json();
};