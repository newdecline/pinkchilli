export const setVhViewport = () => {
    const vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty("--vh", `${vh}px`);
};

export const changeViewport = () => {
    window.addEventListener("resize", setVhViewport);
};